from PIL import Image
import math


def universal_map(A: int, B: int, C: int, D: int, x: int):
    y = (D * x - A * D - C * x + B * C) / (B - A)
    return y


density = "Ñ@#W$9876543210?!abc;:+=-,._ "

length = len(density)

img = Image.open("leo.jpg")
pixels = img.load()
width, height = img.size

output_file = open("ascii_output.txt", "w")

for y in range(height):
    for x in range(width):
        avg = (pixels[x, y][0] + pixels[x, y][1] + pixels[x, y][2]) // 3
        char_index = math.floor(universal_map(0, 255, 0, len(density) - 1, avg))
        output_file.write(density[char_index])
    output_file.write("\n")

output_file.close()
